#define FTIMINGS_PRIVATE
#include <stddef.h>
#include <stdarg.h>
#include <stdlib.h>
#include <string.h>
#include <stdio.h>
#include <math.h>
#include "ftimings.h"

#define __ftimings_node_from_args(node, last_arg, error_return) \
	do { \
		va_list args; \
		va_start(args, last_arg); \
		node = ftimings_node(timer, args); \
		va_end(args); \
		\
		if (!node) { \
			debug("ftimings_get_node() returned NULL"); \
			error_return; \
		} \
	} while(0)

static int looked_for_verbose_debug = 0;
static int verbose_debug = 0;

void debug(const char *format, ...) {
	if(!looked_for_verbose_debug) {
		char *debug = getenv("FTIMINGS_DEBUG");
		if (debug && strncmp(debug, "1", 1) == 0) {
			verbose_debug = 1;
		}
		looked_for_verbose_debug = 1;
	}
	if (verbose_debug) {
		va_list args;
		va_start(args, format);
		vfprintf(stderr, format, args);
		va_end(args);
	}
}

ftimer_node_t* ftimings_node(ftimer_t *timer, va_list args) {
	const char *name;
	ftimer_node_t *node;

	node = ftimings_root_node(timer);

	while((name = va_arg(args, const char*)) != NULL) {
		node = ftimings_node_get_child(node, name);
		if (node == NULL) {
			debug("Cannot descend to %s\n", name);
			ftimings_error(timer, "ftimings_get_node(): Cannot descend to node");
			return NULL;
		}
	}
	return node;
}

double ftimings_in_entries_impl(ftimer_t *timer, ...) {
	va_list args;
	const char *name, *next_name;
	ftimer_node_t *node;

	node = ftimings_root_node(timer);

	va_start(args, timer);
	name = va_arg(args, const char*);
	if (name == NULL) {
		ftimings_error(timer, "ftimings_in_entries(): Missing argument");
		return NAN;
	}
	while((next_name = va_arg(args, const char*)) != NULL) {
		node = ftimings_node_get_child(node, name);
		if (node == NULL) {
			debug("Cannot descend to %s\n", name);
			ftimings_error(timer, "ftimings_in_entries(): Cannot descend to node");
			return NAN;
		}
		name = next_name;
	}

	return ftimings_in_entries_node(timer, node, name);
}

void ftimings_print_impl(ftimer_t *timer, double threshold, ...) {
	ftimer_node_t *node;
	__ftimings_node_from_args(node, threshold, return);

	ftimings_print_node(timer, threshold, node);
}

double ftimings_get_impl(ftimer_t *timer, ...) {
	ftimer_node_t *node;
	__ftimings_node_from_args(node, timer, return NAN);

	return ftimings_get_node(timer, node);
}

double ftimings_get_in_children_impl(ftimer_t *timer, ...) {
	ftimer_node_t *node;
	__ftimings_node_from_args(node, timer, return NAN);

	return ftimings_get_in_children_node(timer, node);
}

double ftimings_get_without_children_impl(ftimer_t *timer, ...) {
	ftimer_node_t *node;
	__ftimings_node_from_args(node, timer, return NAN);

	return ftimings_get_without_children_node(timer, node);
}

double ftimings_since_impl(ftimer_t *timer, ...) {
	ftimer_node_t *node;
	__ftimings_node_from_args(node, timer, return NAN);

	return ftimings_since_node(timer, node);
}
