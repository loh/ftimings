/*
 Copyright 2014 Lorenz Hüdepohl

 This file is part of ftimings.

 ftimings is free software: you can redistribute it and/or modify
 it under the terms of the GNU Lesser General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 ftimings is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU Lesser General Public License for more details.

 You should have received a copy of the GNU Lesser General Public License
 along with ftimings.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef _FTIMINGS_H
#define _FTIMINGS_H

#include "ftimings_generated.h"

#define ftimings_in_entries(timer, ...) ftimings_in_entries_impl(timer, ##__VA_ARGS__, NULL)
double ftimings_in_entries_impl(ftimer_t *timer, ...);

#define ftimings_print(timer, threshold, ...) ftimings_print_impl(timer, threshold, ##__VA_ARGS__, NULL)
void ftimings_print_impl(ftimer_t *timer, double threshold, ...);

#define ftimings_get(timer, ...) ftimings_get_impl(timer, ##__VA_ARGS__, NULL)
double ftimings_get_impl(ftimer_t *timer, ...);

#define ftimings_since(timer, ...) ftimings_since_impl(timer, ##__VA_ARGS__, NULL)
double ftimings_since_impl(ftimer_t *timer, ...);

#define ftimings_get_in_children(timer, ...) ftimings_get_in_children_impl(timer, ##__VA_ARGS__, NULL)
double ftimings_get_in_children_impl(ftimer_t *timer, ...);

#define ftimings_get_without_children(timer, ...) ftimings_get_without_children_impl(timer, ##__VA_ARGS__, NULL)
double ftimings_get_without_children_impl(ftimer_t *timer, ...);

#endif
