/* Copyright 2014 Lorenz Hüdepohl
 *
 * This file is part of ftimings.
 *
 * ftimings is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * ftimings is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with ftimings.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <errno.h>
#include <pthread.h>

#ifdef HAVE_CONFIG_H
#include "config.h"
#endif

static int event_set;

static int tried_papi_init = 0;
static int papi_available = 0;
static int flop_available = 0;
static int cycle_available = 0;

#include <papi.h>

#define papi_try(x, on_error) if ((ret = x) < 0) { \
		fprintf(stderr, "ftimings: %s:%d: " #x ": %s\n", \
				__FILE__, __LINE__, PAPI_strerror(ret)); \
		on_error; \
	}


static void papi_uninit(void) {
	PAPI_shutdown();
}

static int papi_init(void) {
	int ret;

	if (tried_papi_init) {
		return papi_available;
	}

#ifdef _OPENMP
#pragma omp critical
#endif
	do {
		/* Think about it :) */
		if (tried_papi_init) {
			break;
		}

		tried_papi_init = 1;

		event_set = PAPI_NULL;

		papi_try(PAPI_library_init(PAPI_VER_CURRENT), papi_available = 0; break)
		papi_try(PAPI_thread_init(pthread_self), papi_available = 0; break)
		papi_try(PAPI_create_eventset(&event_set), papi_available = 0; break)

		/* FLOP counter
		 */
		papi_try(PAPI_query_event(PAPI_DP_OPS), flop_available = 0)
		else papi_try(PAPI_add_event(event_set, PAPI_DP_OPS), flop_available = 0)
		else {
			flop_available = 1;
		}

		/* Cycle counter
		 */
		papi_try(PAPI_query_event(PAPI_TOT_CYC), cycle_available = 0)
		else papi_try(PAPI_add_event(event_set, PAPI_TOT_CYC), cycle_available = 0)
		else {
			cycle_available = 1;
		}

		/* Start */
		papi_try(PAPI_start(event_set), papi_available = 0; papi_uninit(); break)

		/* PAPI works */
		papi_available = 1;
		atexit(papi_uninit);

	} while(0); /* End of critical region */

	return papi_available;
}

int ftimings_flop_init(void) {
	if (!tried_papi_init) {
		papi_init();
	}

	return flop_available;
}

int ftimings_cycle_init(void) {
	if (!tried_papi_init) {
		papi_init();
	}

	return cycle_available;
}

void ftimings_papi_counters(long long *flop, long long *cycles) {
	long long res[2];
	int i, ret;

	if ((ret = PAPI_read(event_set, &res[0])) < 0) {
		fprintf(stderr, "PAPI_read: %s\n", PAPI_strerror(ret));
		exit(1);
	}

	i = 0;
	if (flop_available) {
		*flop = res[i++];
	} else {
		*flop = 0LL;
	}
	if (cycle_available) {
		*cycles = res[i++];
	} else {
		*cycles = 0LL;
	}
}

#ifdef TEST_PAPI
#include <assert.h>

static void print_papi_counters(int event_set) {
	long long flop_count, cycle_count;
	ftimings_papi_counters(&flop_count, &cycle_count);
	printf("FLOP count: %lld, CPU cycles: %lld\n", flop_count, cycle_count);
	printf("FLOP/cycle: %g\n\n", (double) flop_count / (double) cycle_count);
}

double w;

int main(int argc, char *argv[argc + 1]) {
	int i;

	assert(ftimings_flop_init());

	print_papi_counters(event_set);

	w = 1.5;
	for (i = 0; i < 1 << 27; i++) {
		w = w + 1/w;
	}

	print_papi_counters(event_set);
}
#endif /* TEST_PAPI */
