/* Copyright 2014 Lorenz Hüdepohl
 *
 * This file is part of ftimings.
 *
 * ftimings is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * ftimings is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with ftimings.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifdef HAVE_CONFIG_H
#include "config.h"
#endif

#define _XOPEN_SOURCE
#define _GNU_SOURCE
#include <unistd.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <errno.h>
#include <stdint.h>
#include <inttypes.h>
#include <sys/ioctl.h>
#include <linux/perf_event.h>
#include <linux/hw_breakpoint.h>
#include <asm/unistd.h>

static int perf_available = 0;
static int tried_perf_init = 0;
static int mem_reads_fd = -1, mem_writes_fd = -1;

static inline int perf_event_open(struct perf_event_attr *attr, pid_t pid, int cpu, int group_fd, unsigned long flags) {
   int ret;

   ret = syscall(__NR_perf_event_open, attr, pid, cpu, group_fd, flags);
   return ret;
}

static void ftimings_perf_memory_counters_uninit(void) {
	if (mem_reads_fd > 0) {
		close(mem_reads_fd);
	}
	if (mem_writes_fd > 0) {
		close(mem_writes_fd);
	}
}

int ftimings_perf_memory_counters_init(void) {
	struct perf_event_attr pe;
	int type;
	FILE *ff;

	pid_t pid = -1;
	int cpu = 0;
	unsigned long flags = 0;
	unsigned int event_reads = 0, event_writes = 0;

	if (tried_perf_init) {
		return perf_available;
	}

	tried_perf_init = 1;

	ff = fopen("/sys/devices/uncore_imc/type", "r");
	if (ff == NULL) {
		perror("fopen(\"/sys/devices/uncore_imc/type\", \"r\")");
		return 0;
	}
	if (fscanf(ff, "%d", &type) != 1) {
		perror("Cannot read from /sys/devices/uncore_imc/type");
		return 0;
	}
	fclose(ff);

	ff = fopen("/sys/devices/uncore_imc/events/data_writes", "r");
	if (ff == NULL) {
		perror("fopen(\"/sys/devices/uncore_imc/events/data_writes\", \"r\")");
		return 0;
	}
	if (fscanf(ff, "event=%x", &event_writes) != 1) {
		perror("Cannot read from /sys/devices/uncore_imc/events/data_writes");
		return 0;
	}
	fclose(ff);

	ff = fopen("/sys/devices/uncore_imc/events/data_reads", "r");
	if (ff == NULL) {
		perror("fopen(\"/sys/devices/uncore_imc/events/data_reads\", \"r\")");
		return 0;
	}
	if (fscanf(ff, "event=%x", &event_reads) != 1) {
		perror("Cannot read from /sys/devices/uncore_imc/events/data_reads");
		return 0;
	}
	fclose(ff);

	memset(&pe, 0, sizeof(struct perf_event_attr));
	pe.size = sizeof(struct perf_event_attr);
	pe.type = type;
	pe.config = event_writes;
	pe.disabled = 0;
	pe.inherit = 1;
	pe.exclude_guest = 0;

	mem_writes_fd = perf_event_open(&pe, pid, cpu, -1, flags);
	if (mem_writes_fd == -1) {
		fprintf(stderr, "ftimings: %s:%d: perf_event_open(): %s\n",
				__FILE__, __LINE__, strerror(errno));
		return 0;
	}

	memset(&pe, 0, sizeof(struct perf_event_attr));
	pe.size = sizeof(struct perf_event_attr);
	pe.type = type;
	pe.config = event_reads;
	pe.disabled = 0;
	pe.inherit = 1;
	pe.exclude_guest = 0;

	mem_reads_fd = perf_event_open(&pe, pid, cpu, mem_writes_fd, flags);
	if (mem_reads_fd == -1) {
		fprintf(stderr, "ftimings: %s:%d: perf_event_open(): %s\n",
				__FILE__, __LINE__, strerror(errno));
		close(mem_writes_fd);
		return 0;
	}

	ioctl(mem_writes_fd, PERF_EVENT_IOC_RESET, 0);
	ioctl(mem_writes_fd, PERF_EVENT_IOC_REFRESH, 1);

	ioctl(mem_reads_fd, PERF_EVENT_IOC_RESET, 0);
	ioctl(mem_reads_fd, PERF_EVENT_IOC_REFRESH, 1);

	atexit(ftimings_perf_memory_counters_uninit);

	perf_available = 1;
	return 1;
}


void ftimings_perf_memory_counters(int64_t *reads, int64_t *writes) {
	uint64_t count_reads, count_writes;

	read(mem_reads_fd, &count_reads, sizeof(uint64_t));
	read(mem_writes_fd, &count_writes, sizeof(uint64_t));

	*reads = count_reads * 64;
	*writes = count_writes * 64;
}


#ifdef TEST_PERF

#include <assert.h>
#define mebi (1024L * 1024L)
#define N (512L * mebi / sizeof(int))
volatile int mem[N];

int main() {
	int i, j;
	int64_t count_reads, count_writes;

	assert(ftimings_perf_memory_counters_init());

	ftimings_perf_memory_counters(&count_reads, &count_writes);
	printf("Read %" PRId64 " MiB (%" PRId64 " bytes),\twrote %" PRId64 " MiB (%" PRId64 " bytes) from RAM\n", count_reads / mebi, count_reads, count_writes / mebi, count_writes);
	for (j = 0; j < 4; j++) {

		for(i = 0; i < N; i++) {
			mem[i] = i;
		}
		ftimings_perf_memory_counters(&count_reads, &count_writes);
		printf("Read %" PRId64 " MiB (%" PRId64 " bytes),\twrote %" PRId64 " MiB (%" PRId64 " bytes) from RAM\n", count_reads / mebi, count_reads, count_writes / mebi, count_writes);
	}

	return 0;
}

#endif
