#include <stdlib.h>
#include <stdio.h>
#ifdef NON_TEMPORAL
#include <emmintrin.h>
#endif

#define mebi (1024L * 1024L)

void fill_100_mebi(void) {
	char c = 0x42;
	long i;
	volatile char *p = NULL;

#ifdef NON_TEMPORAL
	__m128i v = _mm_set_epi8(c, c, c, c,
				 c, c, c, c,
				 c, c, c, c,
				 c, c, c, c);
#else
	int j;
#endif

	if ((p = malloc(100L * mebi)) == NULL) {
		perror("malloc()");
		exit(1);
	}

	for (i = 0; i < 100L * mebi; i+=64) {
#ifdef NON_TEMPORAL
		_mm_stream_si128((__m128i *)&p[i+ 0], v);
		_mm_stream_si128((__m128i *)&p[i+16], v);
		_mm_stream_si128((__m128i *)&p[i+32], v);
		_mm_stream_si128((__m128i *)&p[i+48], v);
#else
		for (j = 0; j < 64; j++) {
			p[i+j] = c;
		}
#endif
	}

	free((void *) p);
}

#ifdef TEST_DO_MEMORY_TRANSFER
int main(int argc, char **argv) {
	fill_100_mebi();
	return 0;
}
#endif
