#include <math.h>
#include <stdio.h>
#include <ftimings.h>

double compute_pi(int niterations)
{
    int ii;
    double pi_4 = 0;
    for (ii = 1; ii <= niterations; ii++) {
        pi_4 += pow(-1, ii+1) / (2*ii - 1);
    }
    return pi_4 * 4;
}

int main()
{
    ftimer_t *timer;
    timer = ftimings_create();
    ftimings_enable(timer);
    ftimings_start(timer, "Computation");
    for (int ii=1; ii<=10000; ii++) {
        printf("For i=%d we get pi=%f\n", ii, compute_pi(ii));
    }
    ftimings_stop(timer, "Computation");
    ftimings_print(timer, 0.0);
    return 0;
}
