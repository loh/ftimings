#!/usr/bin/env bash

export CC=gcc
export FC=gfortran
export CXX=g++

export VERBOSE=1

export CFLAGS="-O0"
export CXXFLAGS="-O0"
export FFLAGS="-O0"

#######################################################################
# clean build and "test"                                              #
#######################################################################
rm -rf build

cmake \
    -S . \
    -B build \
    -DCMAKE_BUILD_TYPE=Debug

cmake \
    --build build \
    --target all

cmake \
    --build build \
    --target test
#######################################################################
