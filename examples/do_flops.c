#define NFLOPS 10000000L
#define FLOPS_PER_ITERATION 2
#define N_ITERATIONS (NFLOPS/FLOPS_PER_ITERATION)

double __attribute__((aligned(0x1000))) a[N_ITERATIONS];
double __attribute__((aligned(0x1000))) b[N_ITERATIONS];
double __attribute__((aligned(0x1000))) c[N_ITERATIONS];
double __attribute__((aligned(0x1000))) d[N_ITERATIONS];

/* This should produce 10 million floating point operations,
 * with an arithmetic intensity of
 *
 *  AI = #FLOP / #BYTES = 2 / (4 * sizeof(double)) = 0.0625
 *
 */
void vector_triad(void) {
	int i;

	for (i = 0; i < N_ITERATIONS; i++) {
		a[i] = b[i] + c[i] * d[i];
	}
}

void peak_perf(void) {
	int i;
#ifdef HAVE_AVX
	for (i=0; i < N_ITERATIONS / 4; i++) {
		__asm__ __volatile__(
			"vmulpd %%ymm1, %%ymm2, %%ymm3;"
			"vaddpd %%ymm4, %%ymm5, %%ymm6;"
			: /* No outputs */
			: /* No inputs */
			: "%ymm1", "%ymm2", "%ymm3", "%ymm4", "%ymm5", "%ymm6"
		);
	}
#else
	for (i=0; i < N_ITERATIONS / 2; i++) {
		__asm__ __volatile__(
			"mulpd %%xmm1, %%xmm2;"
			"addpd %%xmm3, %%xmm4;"
			: /* No outputs */
			: /* No inputs */
			: "%xmm1", "%xmm2", "%xmm3", "%xmm4"
		);
	}
#endif
}

#ifdef TEST_DO_FLOPS
int main(int argc, char **argv) {
	vector_triad();
	return 0;
}
#endif
