#include <stdio.h>
#include <stddef.h>
#include <unistd.h>
#include "ftimings.h"

static void error_handler(ftimer_t *timer, void *handle, const char *string) {
	fprintf(stderr, "Got error on timer '%s': %s\n", (char *) handle, string);
}

int main(int argc, char **argv) {
	ftimer_t *timer;
	const char *timername = "mytimer";
	int i = 0;

	timer = ftimings_create();
	ftimings_register_error_handler(timer, error_handler, (void*) timername);

	ftimings_enable(timer);

	ftimings_start(timer, "foobar");

	sleep(1);

	  ftimings_start(timer, "blafasel");
	  sleep(1);
	  ftimings_stop(timer, "blafasel");

	ftimings_stop(timer, "foobar");

	printf("\"foobar\" entry:\n");
	ftimings_print(timer, 0.0, "foobar");
	printf("\n");

	printf("\"foobar->blafasel\" entry:\n");
	ftimings_print(timer, 0.0, "foobar", "blafasel");
	printf("\n");

	printf("Time spent in \"foobar\": %.6f\n", ftimings_get(timer, "foobar"));
	printf("           in \"foobar\"'s children: %.6f\n", ftimings_get_in_children(timer, "foobar"));
	printf("           in \"foobar\" without its children: %.6f\n", ftimings_get_without_children(timer, "foobar"));


	/* Loop with just _start and _stop calls to measure overhead */
	ftimings_start(timer, "overhead");
	for (i = 0; i < 1000000; i++) {
		ftimings_start(timer, "empty");
		ftimings_stop(timer, "empty");
	}
	ftimings_stop(timer, "overhead");


	/* Now with PAPI enabled */
	ftimings_measure_flops(timer, 1);
	ftimings_start(timer, "overhead_papi");
	for (i = 0; i < 1000000; i++) {
		ftimings_start(timer, "empty");
		ftimings_stop(timer, "empty");
	}
	ftimings_stop(timer, "overhead_papi");


	/* Now with PERF enabled */
	ftimings_measure_memory_bandwidth(timer, 1);
	ftimings_measure_flops(timer, 0);
	ftimings_start(timer, "overhead_perf");

	for (i = 0; i < 100000; i++) {
		ftimings_start(timer, "empty");
		ftimings_stop(timer, "empty");
	}
	ftimings_stop(timer, "overhead_perf");


	printf("Full tree:\n");
	ftimings_print_flop_count(timer, 1);
	ftimings_print_memory_transferred(timer, 1);
	ftimings_print(timer, 0.0);
	printf("\n");

	ftimings_sort(timer);
	printf("Full tree sorted:\n");
	ftimings_print(timer, 0.0);
	printf("\n");

	printf("Time spent in \"blafasel\" entries: %.6f\n",
			ftimings_in_entries(timer, "blafasel"));
	printf("Time spent in \"blafasel\" entries below \"foobar\": %.6f\n",
			ftimings_in_entries(timer, "foobar", "blafasel"));

	printf("\nOverhead of ftimings_start() + ftimings_stop() calls:\n  %.2f microseconds\n",
			ftimings_get(timer, "overhead"));
	printf("\nIncluding PAPI (if usable):\n  %.2f microseconds\n",
			ftimings_get(timer, "overhead_papi"));
	printf("\nIncluding PERF (if usable):\n  %.2f microseconds\n",
			10 * ftimings_get(timer, "overhead_perf"));

	printf("\nTime since program start: %.2f seconds\n", ftimings_since(timer));

	ftimings_destroy(timer);

	return 0;
}
